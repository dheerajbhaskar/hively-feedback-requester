package com.osw.hively_feedback;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.otto.Subscribe;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

public class ConnectionFragment extends Fragment {

	TextView txtResponse;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.fragment_connection, container,
				false);

		// set onClickListener for the button
		view.findViewById(R.id.btn_connect).setOnClickListener(clickListener);

		txtResponse = (TextView) view.findViewById(R.id.txtResult);
		return view;
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		BusProvider.getInstance().register(this);
	}

	/**
	 * {@link OnClickListener} for all clicks on this fragment
	 */
	OnClickListener clickListener = new OnClickListener() {

		@Override
		public void onClick(View v) {
			switch (v.getId()) {
			case R.id.btn_connect:
				Toast.makeText(getActivity(), "Connecting ...",
						Toast.LENGTH_SHORT).show();

				doPostRequest();

				break;

			default:
				throw new RuntimeException("Unknow button ID");
			}

		}
	};

	@Subscribe
	public void putResponseTextView(String response) {
		txtResponse.setText(response);
	}

	protected void doPostRequest() {
		// new BackgroundWebRunner().execute();

		AsyncHttpClient client = new AsyncHttpClient();
		RequestParams params = new RequestParams();
		params.put("login", "dheeraj.osw@gmail.com");
		params.put("password", "b5suH2mnt2g4xXCE");
		params.put("referring_url", "");
		params.put("submit", "Let me in!");

		client.post("http://host?method=signIn", params,
				new AsyncHttpResponseHandler() {
					@Override
					public void onSuccess(String response) {
						BusProvider.getInstance().post(response);
					}
				});

	}
}
