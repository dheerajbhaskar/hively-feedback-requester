package com.osw.hively_feedback;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import com.squareup.okhttp.OkHttpClient;

import android.os.AsyncTask;
import android.util.Log;

public class BackgroundWebRunner extends AsyncTask<Void, Void, String> {
	private static final String TAG = "BackgroundWebRunner";
	OkHttpClient client = new OkHttpClient();

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		BusProvider.getInstance().register(this);
	}

	@Override
	protected String doInBackground(Void... params) {
		String response = null;

		try {
			// response = get(new URL(CONST.ENDPOINT));
			byte[] body = "referring_url=&login=dheeraj.osw%40gmail.com&password=b5suH2mnt2g4xXCE&submit=Let+me+in%21"
					.getBytes("UTF-8");
			response = post(new URL(CONST.ENDPOINT), body);
			Log.d(TAG, "response: " + response);
		} catch (IOException e) {
			e.printStackTrace();
			Log.e(TAG, "", e);
		}

		return response;
	}

	@Override
	protected void onPostExecute(String result) {
		super.onPostExecute(result);

		Log.d(TAG, "result isNull " + (result == null));
		BusProvider.getInstance().post(result);
		BusProvider.getInstance().unregister(this);
	}

	/**
	 * get String response of a GET
	 * 
	 * @param url
	 * @return
	 * @throws IOException
	 */
	String get(URL url) throws IOException {

		HttpURLConnection connection = client.open(url);
		InputStream in = null;
		try {
			// Read the response.
			in = connection.getInputStream();
			byte[] response = readFully(in);
			return new String(response, "UTF-8");

		} finally {
			if (in != null) {
				in.close();
			}
		}
	}

	/**
	 * reads an inputstream into a byte array
	 * 
	 * @param in
	 * @return
	 * @throws IOException
	 */
	byte[] readFully(InputStream in) throws IOException {
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		byte[] buffer = new byte[1024];
		for (int count; (count = in.read(buffer)) != -1;) {
			out.write(buffer, 0, count);
		}
		return out.toByteArray();
	}

	// ---------------- POST helpers below this line ---------------------------

	String post(URL url, byte[] body) throws IOException {
		HttpURLConnection connection = client.open(url);
		OutputStream out = null;
		InputStream in = null;
		try {
			// Write the request.
			connection.setRequestMethod("POST");
			out = connection.getOutputStream();
			out.write(body);
			out.close();

			// Read the response.
			if (connection.getResponseCode() != HttpURLConnection.HTTP_OK)
				throw new IOException("Unexpected HTTP response: "
						+ connection.getResponseCode() + " "
						+ connection.getResponseMessage());
			in = connection.getInputStream();
			return readFirstLine(in);
		} finally {
			// Clean up.
			if (out != null) {
				out.close();
			}
			if (in != null) {
				in.close();
			}
		}
	}

	String readFirstLine(InputStream in) throws IOException {
		BufferedReader reader = new BufferedReader(new InputStreamReader(in,
				"UTF-8"));
		return reader.readLine();
	}

}
