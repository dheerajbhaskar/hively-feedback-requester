package com.osw.hively_feedback;

import com.squareup.otto.Bus;

/**
 * Maintains a singleton instance for obtaining the bus.
 * 
 * @author Dheeraj
 * 
 */
public final class BusProvider {
	private static final Bus BUS = new Bus();

	private BusProvider() {
		// no instances
	}

	public static Bus getInstance() {
		return BUS;
	}
}
